/*
 * Copyright (c) 2021 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#ifndef ORCHESTRATION_H_
#define ORCHESTRATION_H_

#include <grpcpp/grpcpp.h>

#include "common.grpc.pb.h"

class DocaOrchestrationImpl : public DocaOrchestration::Service {
	public:
		grpc::Status HealthCheck(grpc::ServerContext *context, const HealthCheckReq *request,
			HealthCheckResp *response) override;

		grpc::Status Destroy(grpc::ServerContext *context, const DestroyReq *request,
			DestroyResp *response) override;
};

#endif /* ORCHESTRATION_H_ */
