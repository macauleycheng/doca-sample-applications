#!/bin/bash
#
# Copyright (c) 2021 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
#
# This software product is a proprietary product of NVIDIA CORPORATION &
# AFFILIATES (the "Company") and all right, title, and interest in and to the
# software product, including all associated intellectual property rights, are
# and shall remain exclusively with the Company.
#
# This software product is governed by the End User License Agreement
# provided with the software product.
#

script_folder_path=`dirname "$0"`
script_path="$0"
params_file_path="${script_folder_path}/east_west_overlay_encryption_params.json"
initiator_ip_addr=`grep '"initiator_ip_addr":' $params_file_path | cut -d ' ' -f2 | cut -d '"' -f2`
receiver_ip_addr=`grep '"receiver_ip_addr":' $params_file_path | cut -d ' ' -f2 | cut -d '"' -f2`
port_num=`grep '"port_num":' $params_file_path | cut -d ' ' -f2 | cut -d '"' -f2`
initiator_conf_file_path=`grep '"initiator_conf_file_path":' $params_file_path | cut -d ' ' -f2 | cut -d '"' -f2`
receiver_conf_file_path=`grep '"receiver_conf_file_path":' $params_file_path | cut -d ' ' -f2 | cut -d '"' -f2`
os_name=`cat /etc/os-release | grep -w 'NAME=' | cut -d '"' -f2 | cut -d ' ' -f1`
pciconf=`ls --color=never /dev/mst/ | grep --color=never '^m.*f0$' | cut -c 3-`
side=$1
mlnx_bf_conf_folder="/etc/mellanox/"
swanctl_conf_folder="/etc/swanctl/conf.d/"
mlnx_bf_conf_file="/etc/mellanox/mlnx-bf.conf"
back_up_folder="/tmp/east_west_backup/"

function usage() {
        echo "Syntax: $script_path --receiver|-r --initiator|-i --destroy|-d [--help|-h]"
        echo "Flag \"--receiver|-r\": Run the script with this flag on the first BlueField"
        echo "Flag \"--initiator|-i\": Run the scirpt with this flag on the second BlueField (after running with --receiver|-r on the first BlueField)"
        echo "Flag \"--destroy|-d\": Run the script with this flag on both BlueFields to revert the IPSec configurations"
}

# Delete from ovs bridges ports that we use
function delete_prev_bridges() {
        PF0=p${port_num}
        PF0_REP=pf${port_num}hpf

        echo "Deleting vxlan-br${port_num} (if exists) and deleting $PF0 and $PF0_REP from all bridges"

        # Delete vxlan-br${port_num} bridge if exists
        ovs-vsctl del-br vxlan-br${port_num} > /dev/null 2>&1

        # Delete PF0 and PF0_REP ports from all other bridges
        for bridge in `ovs-vsctl show |  grep -oP 'Bridge \K\w[^\s]+'`; do
                ovs-vsctl del-port $bridge $PF0 > /dev/null 2>&1
                ovs-vsctl del-port $bridge $PF0_REP > /dev/null 2>&1
        done

}

function ovs_config() {
        delete_prev_bridges

        if [[ "$side" == "--initiator" || "$side" == "-i" ]]; then
                OUTER_REMOTE_IP=$receiver_ip_addr
                OUTER_LOCAL_IP=$initiator_ip_addr
        elif [[ "$side" == "--receiver" || "$side" == "-r" ]]; then
                OUTER_REMOTE_IP=$initiator_ip_addr
                OUTER_LOCAL_IP=$receiver_ip_addr
        fi

        # The port and port representor are set according to the port_num parameter (0/1)
        PF0=p${port_num}
        PF0_REP=pf${port_num}hpf
        ifconfig $PF0 $OUTER_LOCAL_IP/24 up
        ifconfig $PF0_REP up > /dev/null 2>&1

        # Set the mtu of PF0 to 4000 to account for the size of VXLAN headers
        ifconfig $PF0 mtu 4000 up

        # Enable tc offloading
        echo update hw-tc-offload to $PF0 and $PF0_REP
        ethtool -K $PF0_REP hw-tc-offload on > /dev/null 2>&1
        ethtool -K $PF0 hw-tc-offload on

        # Build a VXLAN tunnel over ovs
        # Start openvswitch according to the operating system
        if [[ "$os_name" == "Ubuntu" || "$os_name" == "debian" ]]; then
                service openvswitch-switch start
        elif [[ "$os_name" == "CentOS" || "$os_name" == "rhel" ]]; then
                service openvswitch start
        fi
        ovs-vsctl add-br vxlan-br${port_num}
        ovs-vsctl add-port vxlan-br${port_num} $PF0_REP
        ovs-vsctl add-port vxlan-br${port_num} vxlan11 -- set interface vxlan11 type=vxlan options:local_ip=$OUTER_LOCAL_IP options:remote_ip=$OUTER_REMOTE_IP options:key=100 options:dst_port=4789
        ovs-vsctl set Open_vSwitch . other_config:hw-offload=true
        # Restart openvswitch according to the operating system
        if [[ "$os_name" == "Ubuntu" || "$os_name" == "debian" ]]; then
                service openvswitch-switch restart
        elif [[ "$os_name" == "CentOS" || "$os_name" == "rhel" ]]; then
                service openvswitch restart
        fi
        ovs-vsctl show
}

# Write IPSEC_FULL_OFFLOAD="yes" in /etc/mellanox/mlnx-bf.conf if argument is 1, else revert to previous mlnx-bf.conf file that was backed
function config_offload() {
        if (( $1 == 1 )); then
                # Backup mlnx_bf_conf_file to restore it in the future
                mkdir $back_up_folder > /dev/null 2>&1
                cp $mlnx_bf_conf_file $back_up_folder > /dev/null 2>&1

                # Replace "no" with "yes" in conf_file
                search_string="IPSEC_FULL_OFFLOAD=\"no\""
                replace_string="IPSEC_FULL_OFFLOAD=\"yes\""
                if  grep -Fxq "$replace_string" $mlnx_bf_conf_file; then
                        : # Do nothing
                elif grep -Fxq "$search_string" $mlnx_bf_conf_file; then
                        sed -i "s/$search_string/$replace_string/" $mlnx_bf_conf_file
                else
                        echo $yes_offload >> $mlnx_bf_conf_file
                fi

        else
                # Restore bf_conf_file
                cp ${back_up_folder}mlnx-bf.conf $mlnx_bf_conf_folder > /dev/null 2>&1
                if [[ $? != 0 ]]; then
                        echo "Failed to restore previous swanct.conf files. The backup file may have been deleted (backup folder path: ${back_up_folder}) or the files were not backed up (to backup the files, need to execute "run -i/-r" before "run -d")."
                fi
        fi
}

function revert_ipsec() {
        # Delete the IPsec rules
        sudo ip x s f
        sudo ip x p f
        sudo systemctl start NetworkManager
        sudo ip addr flush dev p${port_num}
        sudo rm -f /etc/openvswitch/conf.db

        # Restart openvswitch according to the operating system
        if [[ "$os_name" == "Ubuntu" || "$os_name" == "debian" ]]; then
                sudo /etc/init.d/openvswitch-switch restart
        elif [[ "$os_name" == "CentOS" || "$os_name" == "rhel" ]]; then
                systemctl restart openvswitch
                systemctl enable openvswitch
        fi

        # Disabling regex temporarily
        echo "Disabling regex temporarily to configure IPsec full offload."
        systemctl stop mlx-regex
        # Disable IPSEC full offload
        config_offload 0
        # Reload configuration
        echo "Restarting ib driver (/etc/init.d/openibd restart)."
        /etc/init.d/openibd restart
        # Check IPsec none offload
        diff  <(echo "none" ) <(cat /sys/class/net/p${port_num}/compat/devlink/ipsec_mode) > /dev/null 2>&1
        if (( $? != 0 )); then
                cat /sys/class/net/p${port_num}/compat/devlink/ipsec_mode
                echo "Failed, IPSec full offload should be none."
        fi
        # Re-enable regex
        echo "Re-enabling regex."
        systemctl restart mlx-regex

        # restore .swanctl.conf files
        sudo cp ${back_up_folder}*.swanctl.conf $swanctl_conf_folder > /dev/null 2>&1
        if [[ $? != 0 ]]; then
                echo "Failed to restore previous swanct.conf files. The backup file may have been deleted (backup folder path: ${back_up_folder}) or the files were not backed up (to backup the files, need to execute "run -i/-r" before "run -d")."
        fi
        sudo rm -rf $back_up_folder


        echo "Reverted IPsec configurations"
}

function config_conf_files() {

        if [[ "$side" == "--initiator" || "$side" == "-i" ]]; then
                # Backup and delete receiver .conf file for initiator side
                mkdir $back_up_folder > /dev/null 2>&1
                sudo cp $receiver_conf_file_path $back_up_folder 2>&1
                sudo rm -f $receiver_conf_file_path
                # Configure the right IP addresses in the .conf file
                sed -i "s/local_addrs.*/local_addrs = ${initiator_ip_addr}/" $initiator_conf_file_path
                sed -i "s/remote_addrs.*/remote_addrs = ${receiver_ip_addr}/" $initiator_conf_file_path
                sed -i "s/local_ts.*/local_ts = ${initiator_ip_addr}\/24 \[udp\/4789\]/" $initiator_conf_file_path
                sed -i "s/remote_ts.*/remote_ts = ${receiver_ip_addr}\/24 \[udp\/4789\]/" $initiator_conf_file_path
        elif [[ "$side" == "--receiver" || "$side" == "-r" ]]; then
                # Backup and delete initiator .conf file for receiver side
                mkdir $back_up_folder > /dev/null 2>&1
                sudo cp $initiator_conf_file_path $back_up_folder 2>&1
                sudo rm -f $initiator_conf_file_path
                # Configure the right IP addresses in the .conf file
                sed -i "s/local_addrs.*/local_addrs = ${receiver_ip_addr}/" $receiver_conf_file_path
                sed -i "s/remote_addrs.*/remote_addrs = ${initiator_ip_addr}/" $receiver_conf_file_path
                sed -i "s/local_ts.*/local_ts = ${receiver_ip_addr}\/24 \[udp\/4789\]/" $receiver_conf_file_path
                sed -i "s/remote_ts.*/remote_ts = ${initiator_ip_addr}\/24 \[udp\/4789\]/" $receiver_conf_file_path
        fi
}

if [ "$#" -ne 1 ]; then
        echo "Illegal number of arguments"
        usage
        exit 0
fi

if [[ "$1" == "--help" || "$1" == "-h" ]]; then
        usage
        exit 0
fi

if [[ "$1" != "--destroy" && "$1" != "-d" && "$1" != "--initiator" && "$1" != "-i"\
        && "$1" != "--receiver" && "$1" != "-r" ]]; then
        echo "Illegal argument"
        usage
        exit 0
fi

# If command is destroy then revert IPsec configurations
if [[ "$1" == "--destroy" || "$1" == "-d" ]]; then
        revert_ipsec
        exit 0
fi

# Disabling regex temporarily
echo "Disabling regex temporarily to configure IPsec full offload."
systemctl stop mlx-regex
# Enable IPSEC full offload
config_offload 1
# Reload configuration
echo "Restarting ib driver (/etc/init.d/openibd restart)."
/etc/init.d/openibd restart
# Check IPsec full offload
diff  <(echo "full" ) <(cat /sys/class/net/p0/compat/devlink/ipsec_mode) > /dev/null 2>&1
if (( $? != 0 )); then
        cat /sys/class/net/p${port_num}/compat/devlink/ipsec_mode
        echo "IPSec full offload failed. IPSec full offload should be full."
        exit 1
fi
# Re-enable regex
echo "Re-enabling regex."
systemctl restart mlx-regex



# Configure the ovs bridge
ovs_config

# Query OVS VXLAN hw_offload rules
ovs-appctl dpctl/dump-flows type=offloaded

# Disable host PF as the port owner
mlxprivhost -d /dev/mst/mt${pciconf} --disable_port_owner r


# Configure the .conf files
config_conf_files

# Restart the strongswan service
systemctl restart strongswan-starter.service

# Load the strongswan files
swanctl --load-all

# Activate strongwan on the initiator's machines
if [[ "$side" == "--initiator" || "$side" == "-i" ]]; then
        swanctl -i --child bf
fi
