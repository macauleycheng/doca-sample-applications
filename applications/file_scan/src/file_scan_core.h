/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#ifndef FILE_SCAN_CORE_H_
#define FILE_SCAN_CORE_H_

#include <stdbool.h>
#include <stdio.h>

#include <doca_regex.h>
#include <doca_regex_mempool.h>

#define MAX_FILE_NAME (255)
#define MAX_REGEX_PCI_ADDRESS_LEN (32)
#define RXP_DEVICE_TYPE "bf2"
#define JOB_RESPONSE_SIZE (32)
#define DEFAULT_MEMPOOL_SIZE (1024)
#define MAX_MATCHES_PER_JOB (255)
#define BF2_REGEX_JOB_LIMIT (1024 * 16) /* 16KB */
#define REGEX_QP_INDEX (0) /* This apps uses 1 QP with index = 0. */

struct file_scan_config {
	char rules_file_path[MAX_FILE_NAME];
	char pci_address[MAX_REGEX_PCI_ADDRESS_LEN];
	char *data_buffer;
	long int data_buffer_len;
	uint16_t nb_qps;           /* Number of QPs to use. */
	uint16_t nb_overlap_bytes; /* Overlap bytes for huge jobs. */
	uint32_t chunk_size;       /* Input chunk size (0 == all). */
	uint32_t total_matches;    /* Holds the total number of matches. */
	uint32_t mempool_size;     /* RegEx memory pool size. */
	FILE *csv_fp;

	struct doca_regex_device *regex_dev;
	struct doca_regex *doca_regex;
	struct doca_regex_mempool *matches_mp;
	struct doca_regex_buffer file_buffer;
	struct doca_regex_job_request job_request;
};

void register_file_scan_params(void);

void file_scan_init(struct file_scan_config *app_cfg);

void file_scan_cleanup(struct file_scan_config *app_cfg);

#endif /* FILE_SCAN_CORE_H_ */
