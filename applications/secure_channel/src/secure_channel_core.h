/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#ifndef SECURE_CHANNEL_CORE_H_
#define SECURE_CHANNEL_CORE_H_

#include <doca_comm_channel.h>

#define MAX_MSG_SIZE 1024

enum secure_channel_ep_mode {
	NO_VALID_INPUT = 0, /* CLI argument is not valid */
	CLIENT,		    /* Run end-point as client */
	SERVER		    /* Run end-point as server */
};

struct secure_channel_config {
	enum secure_channel_ep_mode mode; /* Mode of operation */
	char client_msg[MAX_MSG_SIZE];	  /* Client message to send */
	int nb_send_msg;		  /* Number of messages to send */
};

doca_error_t secure_channel_client(struct doca_comm_channel_ep_t *ep,
			   struct doca_comm_channel_addr_t **peer_addr,
			   struct secure_channel_config *cfg);

doca_error_t secure_channel_server(struct doca_comm_channel_ep_t *ep,
				     struct doca_comm_channel_addr_t **peer_addr);

void register_secure_channel_params(void);

void configure_ep_qp_attributes(struct doca_comm_channel_init_attr *attr);

doca_error_t create_secure_channel_ep(struct doca_comm_channel_ep_t **ep,
			      struct doca_comm_channel_init_attr *attr);

#endif /* SECURE_CHANNEL_CORE_H_ */
