/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#ifndef IPS_CORE_H_
#define IPS_CORE_H_

#include <doca_dpi.h>

#include <dpi_worker.h>
#include <offload_rules.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CLIENT_ID 0x1A

#define MAX_FILE_NAME 255

#define NETFLOW_QUEUE_SIZE 1024

extern bool force_quit;

extern struct doca_dpi_ctx *dpi_ctx;

struct ips_config {
	struct application_dpdk_config *dpdk_config;
	char cdo_filename[MAX_FILE_NAME];
	char csv_filename[MAX_FILE_NAME];
	bool create_csv;
	bool print_on_match;
	bool enable_fragmentation;
	int netflow_source_id;
};

void ips_init(const struct application_dpdk_config *dpdk_config,
	struct ips_config *ips_config, struct dpi_worker_attr *dpi_worker);

void ips_destroy(struct application_dpdk_config *dpdk_config, struct ips_config *ips);

void register_ips_params(void);

void signal_handler(int signum);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* IPS_CORE_H_ */
