/*
 * Copyright (c) 2021-2022 NVIDIA CORPORATION & AFFILIATES, ALL RIGHTS RESERVED.
 *
 * This software product is a proprietary product of NVIDIA CORPORATION &
 * AFFILIATES (the "Company") and all right, title, and interest in and to the
 * software product, including all associated intellectual property rights, are
 * and shall remain exclusively with the Company.
 *
 * This software product is governed by the End User License Agreement
 * provided with the software product.
 *
 */

#include <doca_argp.h>
#include <doca_log.h>

#include <dpdk_utils.h>

#include "dns_filter_core.h"

DOCA_LOG_REGISTER(DNS_FILTER);

/*
 *  The main function, which does initialization
 *  of the rules and starts the process of filtering the DNS packets.
 */
int
main(int argc, char **argv)
{
	struct dns_filter_config app_cfg = {0};
	struct application_dpdk_config dpdk_config = {
		.port_config.nb_ports = 2,
		.port_config.nb_queues = 2,
		.port_config.nb_hairpin_q = 4,
		.reserve_main_thread = true,
	};

	/* Init and start parsing */
	struct doca_argp_program_general_config *doca_general_config;
	struct doca_argp_program_type_config type_config = {
		.is_dpdk = true,
		.is_grpc = false,
	};

	app_cfg.dpdk_cfg = &dpdk_config;
#ifdef GPU_SUPPORT
	/* Enable calling DPDK-GPU functions */
	dpdk_config.pipe.gpu_support = true;
	/* Enable host pinned mempool */
	dpdk_config.pipe.is_host_mem = true;
#endif
	/* Parse cmdline/json arguments */
	doca_argp_init("dns_filter", &type_config, &app_cfg);
	register_dns_filter_params();
	doca_argp_start(argc, argv, &doca_general_config);

	/* Update queues and ports */
	dpdk_init(&dpdk_config);

	/* Init dns filter */
	dns_filter_init(&app_cfg);

	/* Trigger threads (DNS workers) and start processing packets, one thread per queue */
	dns_worker_lcores_run(&app_cfg);

	/* Wait all threads to be done */
	rte_eal_mp_wait_lcore();

	/* Closing and releasing resources */
	dns_filter_destroy(&app_cfg);

	/* DPDK cleanup */
	dpdk_fini(&dpdk_config);

	/* ARGP cleanup */
	doca_argp_destroy();

	return 0;
}
